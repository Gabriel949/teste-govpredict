import React from "react";
import Row from "../row/";
import "./style.css";
import Header from "../header";

type PropsType = {
  x: number;
  y: number;
};

type StateType = {
  rows: Array<any>;
};

export default class Table extends React.Component<PropsType, StateType> {
  constructor(props: PropsType) {
    super(props);
    this.state = {
      rows: []
    };

    this.addCollumn = this.addCollumn.bind(this);
  }

  addCollumn(title: string, selectValue: string, options?: Array<string>) {
    const newRows = this.state.rows;
    newRows.push(
      <Row y={11} x={11} title={title} type={selectValue} options={options} />
    );
    this.setState({
      rows: newRows
    });
  }

  render() {
    const rowsBackground = [];
    for (let y = 0; y < this.props.y + 1; y += 1) {
      rowsBackground.push(
        <Row
          y={y}
          x={this.props.x + 1}
          background={true}
          title={""}
          type={""}
        />
      );
    }
    return (
      <React.Fragment>
        <Header addCollumn={this.addCollumn} />
        <div>{rowsBackground}</div>
        <div className="rows">{this.state.rows}</div>
      </React.Fragment>
    );
  }
}
