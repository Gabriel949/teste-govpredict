import React from "react";
import "./style.css";

type PropsType = {
  addCollumn: (
    title: string,
    selectValue: string,
    options?: Array<string>
  ) => void;
};

type StatType = {
  disabled: boolean;
  title: string;
  selectValue: string;
  selectOption: string;
  options: Array<string>;
};

export default class Header extends React.Component<PropsType, StatType> {
  constructor(props: PropsType) {
    super(props);

    this.state = {
      disabled: true,
      title: "",
      selectValue: "",
      selectOption: "",
      options: []
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSelectChange = this.onSelectChange.bind(this);
    this.clearState = this.clearState.bind(this);
  }

  onInputChange(e: any) {
    this.setState(
      {
        title: e.target.value
      },
      () => {
        if (this.state.title && this.state.selectValue) {
          this.setState({
            disabled: false
          });
        } else {
          this.setState({
            disabled: true
          });
        }
      }
    );
  }

  onSelectChange(e: any) {
    this.setState(
      {
        selectValue: e.target.value
      },
      () => {
        if (this.state.title && this.state.selectValue) {
          this.setState({
            disabled: false
          });
        } else {
          this.setState({
            disabled: true
          });
        }
      }
    );
  }

  onInputOptionsChange(e: any) {
    this.setState({
      selectOption: e.target.value
    });
  }

  addOption() {
    const options = this.state.options;
    options.push(this.state.selectOption);
    this.setState({
      options: options,
      selectOption: ""
    });
  }

  clearState() {
    this.setState({
      disabled: true,
      title: "",
      selectValue: "",
      selectOption: "",
      options: []
    });
  }

  render() {
    return (
      <div className="header">
        <input
          className="title"
          type="text"
          placeholder="Column title"
          value={this.state.title}
          onChange={e => this.onInputChange(e)}
        />
        <select
          className="types"
          name="types"
          id="types"
          value={this.state.selectValue}
          onChange={e => this.onSelectChange(e)}
        >
          <option value="">Choose type</option>
          <option value="date">Date</option>
          <option value="select">Select</option>
          <option value="text">Text</option>
          <option value="number">Number</option>
        </select>

        {this.state.selectValue === "select" && (
          <React.Fragment>
            <input
              className="title"
              type="text"
              placeholder="New option"
              value={this.state.selectOption}
              onChange={e => this.onInputOptionsChange(e)}
            />
            <button
              disabled={this.state.selectOption ? false : true}
              onClick={() => this.addOption()}
              className="button"
            >
              Add Option
            </button>
          </React.Fragment>
        )}
        <button
          onClick={() => {
            this.props.addCollumn(
              this.state.title,
              this.state.selectValue,
              this.state.options
            );
            this.clearState();
          }}
          disabled={this.state.disabled}
          className="button"
        >
          Add 10 rows
        </button>
      </div>
    );
  }
}
