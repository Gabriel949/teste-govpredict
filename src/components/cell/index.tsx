import React from "react";
import "./style.css";

type PropsType = {
  key: string;
  y: number;
  x: number;
  background?: boolean;
  title: string;
  type: string;
  options?: Array<string>;
};

type StateType = {
  value: string;
};

export default class Cell extends React.Component<PropsType, StateType> {
  constructor(props: PropsType) {
    super(props);
    this.state = {
      value: this.props.x === 0 ? this.props.title : ""
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value: any) {
    this.setState({
      value: value.target.value
    });
  }

  render() {
    if (this.props.options && this.props.options.length) {
      if (this.props.x === 0) {
        return (
          <input
            className="cell"
            type={this.props.x === 0 ? "text" : this.props.type}
            value={this.state.value}
            onChange={e => this.onChange(e)}
          />
        );
      }
      return (
        <select className="cell" name="types" id="types">
          {this.props.options.map((option, index) => (
            <option value="option" key={index}>
              {option}
            </option>
          ))}
        </select>
      );
    }
    if (!this.props.background) {
      return (
        <input
          className="cell"
          type={this.props.x === 0 ? "text" : this.props.type}
          value={this.state.value}
          onChange={e => this.onChange(e)}
        />
      );
    }

    if (this.props.x === 0) {
      return <span className="cell-background">{this.props.y}</span>;
    }

    if (this.props.y === 0) {
      const alpha = " abcdefghijklmnopqrstuvwxyz".split("");
      return <span className="cell-background">{alpha[this.props.x]}</span>;
    }

    if (this.props.background) {
      return <span className="cell-background"></span>;
    }
  }
}
