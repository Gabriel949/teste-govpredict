import React from "react";
import Cell from "../cell";
import "./style.css";

type Props = {
  y: number;
  x: number;
  background?: boolean;
  title: string;
  type: string;
  options?: Array<string>;
};

const Row = (props: Props) => {
  const cells = [];
  for (let x = 0; x < props.x; x += 1) {
    cells.push(
      <Cell
        key={`${x}-${props.y}`}
        y={props.y}
        x={x}
        background={props.background}
        type={props.type}
        title={props.title}
        options={props.options}
      />
    );
  }
  return <div className={props.background ? "" : "container"}>{cells}</div>;
};
export default Row;
